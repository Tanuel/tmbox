import {TmBox} from "./components/TmBox";
import {TmBoxAlert} from "./components/TmBoxAlert";
import {TmBoxConfirm} from "./components/TmBoxConfirm";
import {TmBoxPrompt} from "./components/TmBoxPrompt";

(global as any).TmBox = TmBox;
(global as any).TmBoxAlert = TmBoxAlert;
(global as any).TmBoxConfirm = TmBoxConfirm;
(global as any).TmBoxPrompt = TmBoxPrompt;
