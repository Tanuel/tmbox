import {CssMap} from "./CssMap";
import {ITmBoxOptions} from "./Options";
import {TmBox} from "./TmBox";

export interface ITmBoxAlertOptions extends ITmBoxOptions {
    /**
     * Css Class for the ok-button
     */
    buttonClass?: string;
    /**
     * button label
     */
    buttonLabel?: string;
    /**
     * Callback when the ok-button is clicked
     */
    onConfirm?: () => void;
}

const defaultOptions: ITmBoxAlertOptions = {
    buttonClass: "",
    buttonLabel: "Ok",
    onConfirm: null,
};

/**
 * tmBoxAlert
 * Display a simple box to confirm. Optimal for alert messages
 */

export class TmBoxAlert extends TmBox {

    protected options: ITmBoxAlertOptions;

    constructor(options?: ITmBoxAlertOptions | string, onConfirmCallback?: () => void) {
        super(options);
        if (typeof options === "string") {
            this.onConfirm = onConfirmCallback;
            this.options = {...defaultOptions, message: options, onConfirm: onConfirmCallback};
        } else if (typeof options === "object") {
            this.options = {...defaultOptions, ...options};
            if (typeof onConfirmCallback === "function") {
                console.warn("Passing a confirm callback as second parameter is only supported if" +
                    " the first parameter is a string and will be ignored. Pass it inside the options instead");
            }
        }

        const buttonLabel = this.options.buttonLabel;
        const buttonClass = CssMap.btn + " " + CssMap.btnConfirm + " " + this.options.buttonClass;
        super.addButton(buttonLabel, buttonClass, this.options.onConfirm);
    }

    set onConfirm(callback) {
        this.options.onConfirm = callback;
    }

    public addButton(): never {
        throw new Error("Adding more buttons is not supported on this class. Use the base class instead.");
    }

    public addButtonElement(): never {
        throw new Error("Invalid call");
    }
}
