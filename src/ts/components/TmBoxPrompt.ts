import {create} from "tmutil";
import {CssMap} from "./CssMap";
import {ITmBoxOptions} from "./Options";
import {TmBox} from "./TmBox";

export interface ITmBoxPromptOptions extends ITmBoxOptions {
    buttonCancelClass: string;
    buttonCancelLabel: string;
    buttonConfirmClass?: string;
    buttonConfirmLabel?: string;
    inputClassName: string;
    inputPlaceholder: string;
    inputType: string;
    inputValue: string;
    onCancel?: () => void;
    onConfirm?: (value) => void;
}

const defaultOptions: ITmBoxPromptOptions = {
    buttonCancelClass: "",
    buttonCancelLabel: "Cancel",
    buttonConfirmClass: "",
    buttonConfirmLabel: "Ok",
    inputClassName: "",
    inputPlaceholder: "",
    inputType: "text",
    inputValue: "",
    onCancel: null,
    onConfirm: null,
};

export class TmBoxPrompt extends TmBox {

    protected options: ITmBoxPromptOptions;

    constructor(
        options?: ITmBoxPromptOptions|string,
        onConfirmCallback?: () => void,
        onCancelCallback?: () => void,
    ) {
        super(options);

        if (typeof options === "string") {
            this.options = {
                ...defaultOptions,
                message: options,
                onCancel: onCancelCallback,
                onConfirm: onConfirmCallback,
            };
        } else if (typeof options === "object") {
            this.options = {...defaultOptions, ...options};
            if (typeof onConfirmCallback === "function") {
                console.warn("Passing a confirm callback as second parameter is only supported if" +
                    " the first parameter is a string and will be ignored. Pass it inside the options instead");
            }
            if (typeof onCancelCallback === "function") {
                console.warn("Passing a cancel callback as second parameter is only supported if" +
                    " the first parameter is a string and will be ignored. Pass it inside the options instead");
            }
        }

        // Input field
        const spec = create("input", {
            className: CssMap.promptInput + " " + this.options.inputClassName,
            placeholder: this.options.inputPlaceholder,
            type: this.options.inputType,
            value: this.options.inputValue,
        });

        // Confirm Button
        const btnConfirm = create("button", {
            className: CssMap.btn + " " + CssMap.btn + " " + CssMap.btnConfirm + " " + this.options.buttonConfirmClass,
            innerHTML: this.options.buttonConfirmLabel,
        });
        btnConfirm.addEventListener("click", (event) => {
            this.destroyByEvent( () => {
                if (typeof this.options.onConfirm === "function") {
                    this.options.onConfirm(spec.value);
                }
                return true;
            }, event);
        });
        super.addButtonElement(btnConfirm);

        // Cancel Button
        const btnCancel = create("button", {
            className: CssMap.btn + " " + CssMap.btn + " " + CssMap.btnCancel + " " + this.options.buttonCancelClass,
            innerHTML: this.options.buttonCancelLabel,
        });
        btnCancel.addEventListener("click", (event) => {
            this.destroyByEvent(() => {
                if (typeof this.options.onCancel === "function") {
                    this.options.onCancel();
                }
                return true;
            }, event);
        });
        super.addButtonElement(btnCancel);

        // Event listeners for enter and escape
        spec.addEventListener("keydown", (e) => {
            const key = e.key;
            switch (key) {
                // Confirm when the user presses enter
                case "Enter":
                    btnConfirm.click();
                    break;
                // Confirm when the user presses escape
                case "Escape":
                    btnCancel.click();
                    break;
                default:
                    break;
            }
        });
        this.special = spec;
    }

    public open(): this {
        super.open();
        (this.special as HTMLInputElement).focus();
        return this;
    }

    public addButton(): never {
        throw new Error("Invalid call");
    }

    public addButtonElement(): never {
        throw new Error("Invalid call");
    }

    set onConfirm(callback) {
        this.options.onConfirm = callback;
    }

    set onCancel(callback) {
        this.options.onCancel = callback;
    }
}
