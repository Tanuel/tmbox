import {CssMap} from "./CssMap";
import {TmBoxConfirm} from "./TmBoxConfirm";

describe("TmBoxConfirm", () => {
    test("construct basic", () => {
       const box = new TmBoxConfirm();
       expect(box.title).toBe("");
       expect(box.message).toBe("");
       box.open();
       expect(box.domElement.classList).toContain(CssMap.wrapperOpen);

       box.title = "test title";
       box.message = "test message";
       box.special = "test special";
       expect(box.title).toBe("test title");
       expect(box.message).toBe("test message");
       expect(box.special).toBe("test special");
    });
});
