import {CssMap} from "./CssMap";
import {TmBox} from "./TmBox";

describe("TmBoxMain", () => {
    test("construct basic", () => {
       const box = new TmBox();
       expect(box.title).toBe("");
       expect(box.message).toBe("");
       expect(box.domElement.classList).toContain(CssMap.wrapper);
       expect(box.domElement.classList).toContain(CssMap.wrapperClosed);
       expect(box.domElement.classList).not.toContain(CssMap.wrapperOpen);
       box.open();
       expect(box.domElement.classList).toContain(CssMap.wrapperOpen);
       expect(box.domElement.classList).not.toContain(CssMap.wrapperClosed);

       box.title = "test title";
       box.message = "test message";
       box.special = "test special";
       expect(box.title).toBe("test title");
       expect(box.message).toBe("test message");
       expect(box.special).toBe("test special");

       box.hide();
       expect(box.domElement.classList).not.toContain(CssMap.wrapperOpen);
       expect(box.domElement.classList).toContain(CssMap.wrapperClosed);
    });

    test("construct string", () => {
        const box = new TmBox("test message");
        expect(box.message).toBe("test message");
    });

    test("construct options", () => {
        const cb = jest.fn();
        const box = new TmBox({
            message: "test message",
            onOpen: cb,
            special: document.createElement("div"),
            title: "test title",
        });
        expect(box.getOptions().title).toBe("test title");
        expect(box.getOption("message")).toBe("test message");
        expect(box.title).toBe("test title");
        expect(box.message).toBe("test message");
        expect(box.special).toBeInstanceOf(HTMLDivElement);
        expect(box.onOpen).toBe(cb);

        box.open();
        expect(cb).toHaveBeenCalled();
    });

    test("buttons", () => {
        const box = new TmBox();
        box.addButton("btn1");
        const btn1 = box.domElement.querySelector<HTMLButtonElement>("button");
        box.open();
        btn1.click();
        expect(btn1.classList).toContain(CssMap.btn);
        // destroy element after button click (default behaviour)
        expect(box.domElement.parentElement).toBeNull();

        // dont destroy after button click
        const cb2 = jest.fn();
        box.addButton("btn2", "btn-2", cb2, false);
        const btn2 = box.domElement.querySelector<HTMLButtonElement>(".btn-2");
        box.open();
        btn2.click();
        expect(btn2.classList).toContain("btn-2");
        expect(btn2.classList).toContain(CssMap.btn);
        expect(cb2).toHaveBeenCalled();
        // destroy element after button click (default behaviour)
        expect(box.domElement.parentElement).toBe(document.body);

        // dont call destroy if callback returns false
        const cb3 = jest.fn(() => false);
        box.addButton("btn3", "btn-3", cb3);
        const btn3 = box.domElement.querySelector<HTMLButtonElement>(".btn-3");
        box.open();
        btn3.click();
        expect(cb3).toHaveBeenCalled();
        expect(box.domElement.parentElement).toBe(document.body);

        const btn4 = document.createElement("button");
        btn4.className = "btn4";
        box.addButtonElement(btn4);
        expect(box.domElement.querySelector(".btn4")).toBe(btn4);
    });

    test("onOpen", () => {
       const box = new TmBox();
       const cb = jest.fn();
       box.onOpen = cb;
       box.open();
       expect(cb).toHaveBeenCalled();
    });
});
