import {create, each} from "tmutil";
import {CssMap} from "./CssMap";
import {ITmBoxOptions} from "./Options";

const defaultOptions: ITmBoxOptions = {
    message: "",
    special: "",
    title: "",
};

export class TmBox {
    public readonly domElement: HTMLDivElement;
    protected buttons: HTMLDivElement;
    protected boxElement: HTMLDivElement;
    protected messageElement: HTMLElement;
    protected titleElement: HTMLElement;
    protected specialElement: HTMLElement;
    protected readonly options: ITmBoxOptions;

    constructor(options?: ITmBoxOptions | string) {
        if (options instanceof Object) {
            this.options = {...defaultOptions, ...options};
        } else {
            this.options = {
                ...defaultOptions,
                message: options || "",
            };
        }
        this.domElement = this.build();
        this.setOptions(this.options);
    }

    get message() {
        return this.messageElement.innerHTML;
    }

    set message(message) {
        this.setOption("message", message);
    }

    get title() {
        return this.titleElement.innerHTML;
    }

    set title(title) {
        this.setOption("title", title);
    }

    get special() {
        return this.options.special;
    }

    set special(special) {
        this.setOption("special", special);
    }

    public getOptions() {
        return {...this.options};
    }

    public setOptions(options: ITmBoxOptions): this {
        each(options, (key: keyof ITmBoxOptions, value) => {
            this.setOption(key, value);
        });
        return this;
    }

    public getOption(name: keyof ITmBoxOptions) {
        return this.options[name];
    }

    public setOption(name: keyof ITmBoxOptions, value: any) {
        this.options[name] = value;
        switch (name) {
            case "title":
                this.titleElement.innerHTML = value;
                break;
            case "message":
                this.messageElement.innerHTML = value;
                break;
            case "special":
                if (typeof this.options.special === "string") {
                    this.specialElement.innerHTML = this.options.special;
                } else if (this.options.special instanceof HTMLElement) {
                    this.specialElement.appendChild(this.options.special);
                }
                break;
            default:
                break;
        }
        return this;
    }

    get onOpen() {
        return this.options.onOpen;
    }

    set onOpen(callback: ITmBoxOptions["onOpen"]) {
        this.options.onOpen = callback;
    }

    public addButton(
        text: string,
        className: string = "",
        callback?: () => void,
        destroyOnClick: boolean = true,
    ): this {
        const btn = create("button", {
            className: CssMap.btn + " " + className,
            innerHTML: text,
        });
        if (destroyOnClick) {
            btn.addEventListener("click", this.destroyByEvent.bind(this, callback));
        } else if (callback) {
            btn.addEventListener("click", callback);
        }
        this.buttons.appendChild(btn);
        return this;
    }

    public addButtonElement(button: HTMLButtonElement): this {
        this.buttons.appendChild(button);
        return this;
    }

    public repositionBox(): this {
        this.boxElement.style.top = (this.domElement.offsetHeight / 2 - this.boxElement.offsetHeight / 2) + "px";
        this.boxElement.style.left = (this.domElement.offsetWidth / 2 - this.boxElement.offsetWidth / 2) + "px";
        return this;
    }

    public destroy() {
        this.domElement.classList.remove(CssMap.wrapperOpen);
        this.domElement.parentNode.removeChild(this.domElement);
    }

    public hide(): this {
        this.domElement.classList.remove(CssMap.wrapperOpen);
        this.domElement.classList.add(CssMap.wrapperClosed);
        this.domElement.style.display = "none";

        return this;
    }

    public open(): this {
        if (this.domElement.parentElement === null) {
            document.body.appendChild(this.domElement);
        }
        this.domElement.classList.add(CssMap.wrapperOpen);
        this.domElement.classList.remove(CssMap.wrapperClosed);
        this.domElement.style.display = "block";
        this.repositionBox();
        const btn = this.buttons.firstElementChild as HTMLButtonElement;
        if (btn) {
            btn.focus();
        }
        if (typeof this.options.onOpen === "function") {
            this.options.onOpen();
        }
        return this;
    }

    protected destroyByEvent(btnCallback, event) {
        if (typeof btnCallback === "function" && btnCallback(event) === false) {
            return;
        }
        this.destroy();
    }

    private build() {
        const wrapper = create<"div">("div", {
            className: CssMap.wrapper + " " + CssMap.wrapperClosed,
            style: {display: "none"},
        });
        const boxElement = this.boxElement = create("div", {className: CssMap.box});
        const titleElement = this.titleElement = create("div", {className: CssMap.title});
        const messageElement = this.messageElement = create("div", {className: CssMap.message});
        const special = this.specialElement = create("div", {className: CssMap.special});
        const buttonContainer = this.buttons = create("div", {className: CssMap.btnContainer});

        boxElement.appendChild(titleElement);
        boxElement.appendChild(messageElement);
        boxElement.appendChild(special);
        boxElement.appendChild(buttonContainer);
        wrapper.appendChild(boxElement);
        document.body.appendChild(wrapper);

        window.addEventListener("resize", this.repositionBox.bind(this));
        return wrapper;
    }
}
