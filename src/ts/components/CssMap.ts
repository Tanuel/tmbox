/**
 * Type declaration for the css map
 */
export interface ITmBoxCssMap {
    box: string;
    btn: string;
    btnCancel: string;
    btnConfirm: string;
    btnContainer: string;
    message: string;
    promptInput: string;
    special: string;
    title: string;
    wrapper: string;
    wrapperClosed: string;
    wrapperOpen: string;
}

const box = "tmBox";
const wrapper = box + "-outer";
const wrapperOpen = box + "-open";
const wrapperClosed = box + "-closed";
const title = box + "-title";
const message = box + "-message";
const btnContainer = box + "-buttons";
const btn = box + "-btn";
const btnCancel = btn + "-cancel";
const btnConfirm = btn + "-confirm";

const special = box + "-special";
const promptInput = box + "-input";

/**
 * Object with css class names to be used to identify the window
 */
export const CssMap: ITmBoxCssMap = {
    box,
    btn,
    btnCancel,
    btnConfirm,
    btnContainer,
    message,
    promptInput,
    special,
    title,
    wrapper,
    wrapperClosed,
    wrapperOpen,
};
