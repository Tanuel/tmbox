export interface ITmBoxOptions {
    /**
     * Message to be displayed below the title
     */
    message?: string;
    /**
     * Title to be displayed aboute the message
     */
    title?: string;
    /**
     * Additional content to be added below the message (can be a HTMLElement)
     */
    special?: HTMLElement|string;
    /**
     * Display to be called when the box is opened.
     */
    onOpen?: () => void;
}
