import {CssMap} from "./CssMap";
import {ITmBoxOptions} from "./Options";
import {TmBox} from "./TmBox";

export interface ITmBoxConfirmOptions extends ITmBoxOptions {
    buttonCancelClass?: string;
    buttonCancelLabel?: string;
    buttonConfirmClass?: string;
    buttonConfirmLabel?: string;
    onCancel?: () => void;
    onConfirm?: () => void;
}

const defaultOptions: ITmBoxConfirmOptions = {
    buttonCancelClass: "",
    buttonCancelLabel: "Cancel",
    buttonConfirmClass: "",
    buttonConfirmLabel: "Ok",
    onCancel: null,
    onConfirm: null,
};

/**
 * Display a Box where the user has the option to confirm or cancel an operation
 */
export class TmBoxConfirm extends TmBox {

    protected options: ITmBoxConfirmOptions;

    constructor(
        options?: ITmBoxConfirmOptions|string,
        onConfirmCallback?: () => void,
        onCancelCallback?: () => void,
    ) {
        super(options);

        if (typeof options === "string") {
            this.options = {
                ...defaultOptions,
                message: options,
                onCancel: onCancelCallback,
                onConfirm: onConfirmCallback,
            };
        } else if (typeof options === "object") {
            this.options = {...defaultOptions, ...options};
            if (typeof onConfirmCallback === "function") {
                console.warn("Passing a confirm callback as second parameter is only supported if" +
                    " the first parameter is a string and will be ignored. Pass it inside the options instead");
            }
            if (typeof onCancelCallback === "function") {
                console.warn("Passing a cancel callback as second parameter is only supported if" +
                    " the first parameter is a string and will be ignored. Pass it inside the options instead");
            }
        }

        const buttonConfirmLabel = this.options.buttonConfirmLabel;
        const buttonConfirmClass = CssMap.btn + " " + CssMap.btnConfirm + " " + this.options.buttonConfirmClass;
        super.addButton(buttonConfirmLabel, buttonConfirmClass, this.options.onConfirm);

        const buttonCancelLabel = this.options.buttonCancelLabel;
        const buttonCancelClass = CssMap.btn + " " + CssMap.btnCancel + " " + this.options.buttonCancelClass;
        super.addButton(buttonCancelLabel, buttonCancelClass, this.options.onCancel);
    }

    public addButton(): never {
        throw new Error("Invalid call");
    }

    public addButtonElement(): never {
        throw new Error("Invalid call");
    }

    set onConfirm(callback) {
        this.options.onConfirm = callback;
    }

    set onCancel(callback) {
        this.options.onCancel = callback;
    }
}
