document.addEventListener('DOMContentLoaded', () => {
    /**
     * Default Box
     */
    // let tmb = new TmBox({
    //     title: 'HelloBox',
    //     message: 'This is a TmBox'
    // });
    // tmb.addButton('Nice Button', 'myButton-yo');
    // tmb.open();

    /**
     * Alert Box
     */
    document.getElementById("showAlertBox").addEventListener("click", () => {
        let alertBox = new TmBoxAlert({
            title: "TmBoxAlert",
            message: "This is an Alert Box",
            onConfirm: () => {
                alert('Accepted -> calling onConfirm callback');
            }
        });
        alertBox.open();
    });

    /**
     * Confirm Box
     */
    document.getElementById("showConfirmBox").addEventListener("click", () => {
        let confirmBox = new TmBoxConfirm({
            title: "TmBoxConfirm",
            message: "This is a Confirm Box",
            onConfirm: () => {
                alert('Accepted -> calling onConfirm callback');
            },
            onCancel: () => {
                alert('Canceled -> calling onCancel callback')
            }
        });
        confirmBox.open();
    });
    /**
     * Prompt Box
     */
    document.getElementById("showPromptBox").addEventListener("click", () => {
        let promptBox = new TmBoxPrompt({
            title: "TmBoxPrompt",
            message: "This is a Prompt Box",
            inputPlaceholder: "Put something here",
            onConfirm: value => {
                alert('Accepted -> passing result to onConfirm callback: ' + value);
            },
            onCancel: () => {
                alert('Canceled -> no result');
            }
        });
        promptBox.open();
    });

});