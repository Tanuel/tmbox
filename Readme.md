[![Powered by Tanuel](https://img.shields.io/badge/Powered%20by-Tanuel-b22.svg)](https://gitlab.com/Tanuel)
[![Documentation](https://img.shields.io/badge/-Documentation-blueviolet.svg)](https://tanuel.gitlab.io/tmbox)
[![Build Pipeline](https://gitlab.com/Tanuel/tmbox/badges/master/pipeline.svg)](https://gitlab.com/Tanuel/tmbox/pipelines)
[![npm](https://img.shields.io/npm/v/tmbox.svg)](https://www.npmjs.com/package/tmbox)
[![npm](https://img.shields.io/npm/dt/tmbox.svg?logo=npm)](https://www.npmjs.com/package/tmbox)

# TmBox

[➡ Project Page ⬅](https://tanuel.gitlab.io/tmbox/)

[➡ Full API Reference (TypeDoc) ⬅](https://tanuel.gitlab.io/tmbox/typedoc/)

## Install

Using yarn

    yarn add tmbox

using npm
    
    npm install tmbox

## Usage

[More examples here](https://tanuel.gitlab.io/tmwindow)

```javascript

/**
 * Alert Box
 */
const alertBox = new TmBoxAlert({
    title: "TmBoxAlert",
    message: "This is an Alert Box",
    onConfirm: () => {
        alert('Accepted -> calling onConfirm callback');
    }
});
alertBox.open();

/**
 * Confirm Box
 */
const confirmBox = new TmBoxConfirm({
    title: "TmBoxConfirm",
    message: "This is a Confirm Box",
    onConfirm: () => {
        alert('Accepted -> calling onConfirm callback');
    },
    onCancel: () => {
        alert('Canceled -> calling onCancel callback')
    }
});
confirmBox.open();

/**
 * Prompt Box
 */
const promptBox = new TmBoxPrompt({
    title: "TmBoxPrompt",
    message: "This is a Prompt Box",
    inputPlaceholder: "Put something here",
    onConfirm: value => {
        alert('Accepted -> passing result to onConfirm callback: ' + value);
    },
    onCancel: () => {
        alert('Canceled -> no result');
    }
});
promptBox.open();
```
